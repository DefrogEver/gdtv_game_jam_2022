using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscBladeBouncing : MonoBehaviour
{
    private Rigidbody2D _rb2D;

    private Vector3 _lastVelocity;

    [Header("PARAMS")]
    public float timeToStart;
    public float actualSpeed;
    public float minSpeed, maxSpeed;

    #region Init

    private void Awake()
    {
        TryGetComponent(out _rb2D);
    }

    private void Start()
    {
        Invoke(nameof(StartMoving), timeToStart);
    }

    void StartMoving()
    {
        actualSpeed = SetRandomSpeed(minSpeed, maxSpeed);
        _rb2D.AddForce(SetRandomTrajectory().normalized * actualSpeed);
    }

    #endregion

    #region Update

    private void Update()
    {
        _lastVelocity = _rb2D.velocity;
    }

    #endregion

    #region Wall & Player Detection

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StopMoving();
            GameOver();
            print("Player death");
        }
    }

    private void GameOver()
    {
        MenuController.Instance.ShowMenu(MenuController.Instance.gameOverMenu);
        GameManager.Instance.TimerPause();
    }

    void StopMoving()
    {
        _rb2D.velocity = Vector2.zero;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Wall"))
        {

#if UNITY_EDITOR
            float angle = GetCurrentAngle();
            print("<color=magenta>Collision with: " + collision.gameObject.name + "</color> | " + "<color=cyan>Current angle:" + angle + "</color>");
#endif

            float speed = _lastVelocity.magnitude;
            Vector3 direction = Vector3.Reflect(_lastVelocity.normalized, collision.contacts[0].normal);
            _rb2D.velocity = direction * Mathf.Max(speed, 0f);

            SetRandomTrajectory();
        }
    }

    private float GetCurrentAngle()
    {
        return Vector2.SignedAngle(Vector2.up, _rb2D.velocity);
    }

    #endregion

    #region Utilities

    private float SetRandomSpeed(float min, float max)
    {
        float newSpeed = Random.Range(min, max);

#if UNITY_EDITOR
        Debug.Log("New Speed :" + newSpeed);
#endif

        return newSpeed;
    }

    private Vector2 SetRandomTrajectory()
    {
        Vector2 newDirection = Vector2.zero;
        newDirection.x = Random.Range(-1f, 1f);
        newDirection.y = -1f;

#if UNITY_EDITOR
        Debug.Log("<color=yellow>New trajectory :" + newDirection + "</color>");
#endif

        return newDirection;
    }

    #endregion

    public void StartDiscMove()
    {
        Invoke(nameof(StartMoving), timeToStart);
    }
}
