using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI timerTMP;
    public float time;

    private float msec, sec, min;

    public void StartWatch()
    {
        StartCoroutine(nameof(StopWatch));
    }

    public void PauseWatch()
    {
        StopCoroutine(nameof(StopWatch));
    }

    public void ResetWatch()
    {
        time = 0;
        timerTMP.text = "Time: " + "00:00:00";
    }

    IEnumerator StopWatch()
    {
        while (true)
        {
            time += Time.deltaTime;
            msec = (int)((time - (int)time) * 100);
            sec = (int)(time %60);
            min = (int)(time /60 %60);

            timerTMP.text = "Time: " + string.Format("{0:00}:{1:00}:{2:00}",min,sec,msec);
            yield return null;
        }
    }

}
