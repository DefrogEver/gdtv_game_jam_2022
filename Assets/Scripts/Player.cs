using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [Header("MOVEMENT")]
    public float moveSpeed;
    private Vector2 _moveInput;
    private Rigidbody2D _rb2D;
    private bool isMoving;

    [Header("DASH")]
    private float _lastDashTime = float.MinValue;
    private bool isDashing;
    public float dashDelay = 3f;
    public float dashPower = 24f;
    //public bool canDash => Time.time > _lastDashTime + dashDelay;

    [Header("INPUTS")]
    public InputActionReference inputMovement;
    public InputActionReference inputDash;

    [Header("EFFECT")]
    public ParticleSystem dustFX;

    [Header("ANIMATIONS")]
    public Animator animator;
    public AnimationClip idleAnimClip, walkAnimClip, dashAnimClip;

    private void Awake()
    {
        TryGetComponent(out _rb2D);
    }

    private void Update()
    {
        /*if (IsInputActionPressed(inputDash))
        {
            animator.Play(dashAnimClip.name);
            print("DASH");
        }
        if (IsInputActionReleased(inputDash))
        {
            isDashing = false;
        }*/


        if (IsInputActionPressed(inputMovement))
        {
            ProcessMoveInput();

            isMoving = true;
            
            animator.Play(walkAnimClip.name);

            CreateDust();
        }
        if (IsInputActionPerformed(inputMovement))
        {
            ProcessMoveInput();

            CreateDust();
        }
        if (IsInputActionReleased(inputMovement))
        {
            isMoving = false;

            animator.Play(idleAnimClip.name);
        }
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            //_rb2D.MovePosition(_rb2D.position + _moveInput * moveSpeed * Time.fixedDeltaTime);
            _rb2D.velocity = _moveInput.normalized * moveSpeed;
        }
        else
        {
            _rb2D.velocity = Vector2.zero;
        }
    }

    private void ProcessMoveInput()
    {
        _moveInput = inputMovement.action.ReadValue<Vector2>();
    }

    private bool IsInputActionPressed(InputActionReference inputRef)
    {
        return inputRef != null && inputRef.action.WasPressedThisFrame();
    }
    private bool IsInputActionPerformed(InputActionReference inputRef)
    {
        return inputRef != null && inputRef.action.WasPerformedThisFrame();
    }
    private bool IsInputActionReleased(InputActionReference inputRef)
    {
        return inputRef != null && inputRef.action.WasReleasedThisFrame();
    }

    private void CreateDust()
    {
        dustFX.Play();
    }
}
