using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GizmoCollider2D : MonoBehaviour
{
    [SerializeField]
    private Color _color;
    [SerializeField]
    private bool _showGizmos;

    private BoxCollider2D _collider2D;

    private void OnDrawGizmos()
    {
        if (_showGizmos)
        {
            TryGetComponent(out _collider2D);

            Gizmos.color = _color;
            Gizmos.DrawWireCube(_collider2D.bounds.center, _collider2D.bounds.size);
        }        
    }
}
