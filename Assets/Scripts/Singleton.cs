using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    [SerializeField]
    private bool _dontDestroyOnLoad = true;

    private static T _instance;

    public static T Instance 
    {
        get 
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<T>();
                if (_instance == null)
                {
                    GameObject go = new GameObject("Instance of " + typeof(T).Name);
                    _instance = go.AddComponent<T>();
                }
            }
 
            return _instance;
        }
    }

    public Transform _transform { get; private set; }
    private void Awake()
    {
        if (_instance == null)
        {
            _instance = (T)this;
            _transform = transform;

            if (_dontDestroyOnLoad)
            {
                DontDestroyOnLoad(gameObject);
            }

            OnAwake();
        }
        else if (Instance != this)
        {
            Destroy(gameObject); // prevent duplicates
        }
    }

    protected virtual void OnAwake(){}
}
