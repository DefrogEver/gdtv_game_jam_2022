using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum States
{
    None,
    Menu,
    Game,
	GameOver
}

public class GameManager : Singleton<GameManager>
{
	private Timer _timer;
	private  DiscBladeBouncing _discBouncing;
    private States _state = States.Menu;

	/// <summary>
	/// Actual state of the game
	/// Make the transition from one state to another
	/// </summary>
	public States State
	{
		get
		{
			return _state;
		}

		set
		{
			if (_state == value)
			{
				return;
			}

			switch (value)
			{
				default:
				case States.None:
					break;
				case States.Menu:
					//Cursor.visible = true;
					break;
				case States.Game:
					//Cursor.visible = false;
					break;
				case States.GameOver:
					// To do: Show game over menu
					break;
			}

			_state = value;
			print(_state);
		}
	}

    protected override void OnAwake()
    {
        base.OnAwake();
		State = States.Menu;
	}

    private void Start()
    {
		TryGetComponent(out _timer);
		TryGetComponent(out _discBouncing);
	}

	public void StartDiscMove()
	{
		_discBouncing.StartDiscMove();
	}

	public void TimerStart() { _timer.StartWatch(); }
	public void TimerPause() { _timer.PauseWatch(); }
	public void TimerReset() { _timer.ResetWatch(); }
}
