using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySceneManager = UnityEngine.SceneManagement.SceneManager;

public class MenuController : Singleton<MenuController>
{
	public GameObject pauseMenu;
	public GameObject gameOverMenu;

	public AudioClip buttonSound;
	public AudioClip buttonStartSound;

	private float _previousScale = 1f;

	private bool inGame = false;

    #region Change Scene

    public void LoadScene(string sceneName)
	{
        if (sceneName == "GAME")
        {
			inGame = true;
		}
		else if(sceneName == "MENU")
        {
			inGame = false;
        }

		UnitySceneManager.LoadScene(sceneName);
	}

	public void LoadScene(int sceneNumber)
	{
		UnitySceneManager.LoadScene(sceneNumber);
	}

	/// <summary>
	/// Restart Current Scene
	/// </summary>
	public void LoadScene()
	{
		int currentScene = UnitySceneManager.GetActiveScene().buildIndex;
		UnitySceneManager.LoadScene(currentScene);
	}

	#endregion

	#region Menus

	private bool _show = false;

    private void Awake()
    {
		HideMenu(pauseMenu);
    }

    /*private void Start()
    {
		GameManager.Instance.State = States.Menu;
	}*/

    private void Update()
    {
		/*if (pauseMenu != null)
        {
			if (Input.GetKeyDown(KeyCode.P) && inGame)
			{
				_show = !_show;
				PauseMenu(_show);
			}
		}*/
    }

    public void PauseMenu(bool active)
    {

        if (active) // Pause Game
		{
			print("Pause");
			PauseTime();
        }
        else if (!active) // Continue Game
		{
			print("Unpause");
			ResumeTime();
		}

		pauseMenu.SetActive(active);
    }

	public void ShowMenu(GameObject gameObject)
	{
		gameObject.SetActive(true);
	}

	public void HideMenu(GameObject gameObject)
	{
		gameObject.SetActive(false);
	}

	#endregion

	#region Buttons

#if !UNITY_WEBGL
    public void Quit()
    {
        Application.Quit();
    }
#endif

	public void GoToUrl(string URL)
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
			Application.ExternalEval("window.open(\"" + URL + " \",\"_blank\")");
        }
        else
        {
			Application.OpenURL(URL);
        }
    }

	public void SoundBasicButton()
    {
		AudioManager.Instance.PlayOnce(buttonSound);
    }

	public void SoundStartButton()
	{
		AudioManager.Instance.PlayOnce(buttonStartSound);
	}

	#endregion

	public void ResumeTime()
    {
		//Time.timeScale = 1;

		Time.timeScale = _previousScale;
	}

	public void PauseTime()
    {
		//Time.timeScale = 0;

		_previousScale = Time.timeScale;
		Time.timeScale = 0;
	}
}
