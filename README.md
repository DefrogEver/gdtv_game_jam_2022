# GDTV: GAME JAM 2022

A game project for the GameDev.tv Jam !

## Description
A little survival 2D game created spacialy for the [Game Dev TV Jam 2022.](https://itch.io/jam/gamedevtv-jam-2022)

Click [here](https://en.wikipedia.org/wiki/Game_jam) if you don't know what is about.

[The project on itch.io](https://defrog.itch.io/gdtv_game_jam_2022)

## Controls
- WASD or arrow keys to move

## Technical infos
- Unity version: 2021.3.3f1
- Programming language : C#
- Text Editor: Visual Studio 2022

## Credits
- Art & Prog: [Paul-Jean Formenti (Defrog)](https://twitter.com/DefrogEver)
- Fonts: ["Montserrat Alternates" (from google font)](https://fonts.google.com/specimen/Montserrat+Alternates#standard-styles)
- Music: [Pixelland by Kevin MacLeod](https://incompetech.filmmusic.io/song/4222-pixelland)
- Sound fx: [Leszek_Szary](https://freesound.org/people/Leszek_Szary/sounds/146718/)
- Sound fx: [Daniel Lucas](https://freesound.org/people/danlucaz/sounds/517761/)

## License
- MIT License.
